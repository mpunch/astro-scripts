#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Prints out the Sun set/rise times from the hardcoded date for a year,
and gives the total of the dark-time.

By default, gives astronomical, but with "n" as argument gives Nautical,
"c" for civil, and "s" for actual Sun set/rise.

It takes the longitude to get the local midnight, and then calculates the rise/set for that.
So for Vaxjo this would be ~23h, so the line for 2020-10-20 (for example) gives 
the Sunset for the evening of the 20th, and the rise on the morning of the 21st.

For places to the West of the Meridian, probably gives the day before (unexpected), 
so will need to treat this case later. 
"""

import ephem
import time
import sys

#Make an observer
vaxjo      = ephem.Observer()

if len(sys.argv)>1 and sys.argv[1][0].lower() in ['n','c','s']: # There is an argument
    first_char = sys.argv[1][0].lower()
    if first_char == 'n':  #Nautical twilight
        horizon = '-12'
        cent = True
        description = 'Nautical night'
    elif first_char == 'c': #Civil twilight
        horizon = '-6'
        cent = True
        description = 'Civil night'
    elif first_char == 's': #Sunrise/Sunset 
        horizon = '0'
        cent = False
        description = 'Official night (Sunset/rise)'
else: # Astro by default
    horizon = '-18'
    cent = True
    description = 'Astronomical night'
    
print(description)

# Decimal lat-long coordinates are 56.87767, 14.80906.
# 56.8790° N, 14.8059° E
vaxjo_lat, vaxjo_lon = 56.87767, 14.80906
vaxjo.lat  = str(vaxjo_lat)      #Note that lat should be in string format
vaxjo.lon  = str(vaxjo_lon) #Note that lon should be in string format
#vaxjo.lat = str(-22)


#Elevation, in metres
vaxjo.elev = 20

#To get U.S. Naval Astronomical Almanac values, use these settings
vaxjo.pressure= 0
#vaxjo.horizon = '-0:34'

#PyEphem takes and returns only UTC times. 15:00 is noon in Fredericton
date_start = "2020-06-21"
date_start += " "
# Always do this around midnight, to get set/rise
# 24*60*60 is seconds in day, 360 is degrees in circle, of course
local_midnight = time.strftime("%H:%M:%S", time.gmtime(24*60*60*(360-vaxjo_lon)/360.)) 
date_start += local_midnight
print(date_start)

tot = 0
for day in range(365):
    vaxjo.date = date_start
    vaxjo.date += day   
    # Latitude and longitude coordinates for Växjö, Sweden: 
    #sunrise=vaxjo.previous_rising(ephem.Sun()) #Sunrise
    #noon   =vaxjo.next_transit   (ephem.Sun(), start=sunrise) #Solar noon
    #sunset =vaxjo.next_setting   (ephem.Sun()) #Sunset
    
    #We relocate the horizon to get twilight times
    vaxjo.horizon = horizon #-6=civil twilight, -12=nautical, -18=astronomical
    try:
        end_twilight=vaxjo.previous_setting   (ephem.Sun(), use_center=cent) #End nautical twilight
        beg_twilight=vaxjo.next_rising(ephem.Sun(), use_center=cent) #Begin nautical twilight
        night_length_h = 24*(beg_twilight-end_twilight)
    except:
        night_length_h = 0
        beg_twilight, end_twilight = vaxjo.date, vaxjo.date 
    tot += night_length_h
    
    print(f"{str(vaxjo.date).split(' ')[0]:10s}    {night_length_h:.2f}: ",
          f"{str(end_twilight)[-8:]}-{str(beg_twilight)[-8:]}")

print("Total "+description+f" hours in Vaxjo {tot:.2f}")
