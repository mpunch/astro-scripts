# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Check if Sun and/or Moon are up
# ## Script which calls IsSet function

# +
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Checks if the Sun or Moon is up.

By default for sun, gives astronomical, but -H argument allows one of {a,n,c,o} for
  Astro, Nautical, Civil, Official (actual Sun set/rise).

Lots more fun things at https://chrisramsay.co.uk/posts/2017/03/fun-with-the-sun-and-pyephem/
"""

import sys
# -

from astro_mp_lib import rise_set

# Here, if running the script from the command line, do it and then exit
try:
    __IPYTHON__ # If running in notebook, or as script
except NameError:
    # Not in IPython -> as script
    rise_set()
    sys.exit()

# # At this point, we are sure to not be in a script.
# ## So, can test things
# Don't mind the ```exception``` when getting the help...

print("Examples")
print('-'*80)
#print("In IPython")
# Can pass arguments directly
rise_set(['-S'])
print('-'*80)
rise_set(['-h'])

# ## Try your own command line (edit the between the command = "..." below)

# Make your own command line, to find the args
command = "command -Hn"
#command = "command -h"

args = command.split('-')
args = ['-'+_ for _ in args][1:]
print(args)

rise_set(args)

rise_set(['-d 20210601','-Hn'])


