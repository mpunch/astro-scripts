# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import sys

def rise_set(args=sys.argv[1:]):
    """
    Checks if the Sun or Moon is up.
    
    Input:
        uses sys.argv for inputs
    Ouptuts:
        prints outputs
    Input:
    By default for sun, gives astronomical, but -H argument allows one of {a,n,c,o} for
      Astro, Nautical, Civil, Official (actual Sun set/rise).

    Lots more fun things at https://chrisramsay.co.uk/posts/2017/03/fun-with-the-sun-and-pyephem/
    """
    import ephem
    import time
    import sys
    import math
    import argparse, textwrap

    from dateutil import parser as dateparse
    
    #print("***",args)

    parser = argparse.ArgumentParser()
    parser.add_argument('-S','--Sun',action="store_true",default=False,
                        help="Show set/rise for Sun "
                             "\n (default False) (for --Horizon)")
    parser.add_argument('-M','--Moon',action="store_true",default=False,
                        help="Show set/rise for Moon "
                             "\n (default False) (for Horizon=0)")
    parser.add_argument('-H','--Horizon', choices=['a', 'n', 'c', 'o'], default='a', 
                        help="For Sun: Astro, Nautical, Civil, Official (deflt 'a')")

    parser.add_argument('-d','--date', type=str, default=time.strftime("%Y-%m-%d %H:%M:%S ", 
                        time.gmtime()), help='date/time to check (free format, UTC, default Now)')
    parser.add_argument('-lon','--longitude', type=float, default=None,
                        help='default Vaxjo (14.80906, +ve E)')
    parser.add_argument('-lat','--latitude', type=float, default=None,
                        help='default Vaxjo (56.87767, +ve N)')
    parser.add_argument('-elev','--elevation', type=float, default=200.,
                        help='default (200m)')

    args = parser.parse_args(args=args)
    
    if '-h' in args:
        return

    if not (args.Sun or args.Moon): #If neither, then force both!
      args.Sun, args.Moon = True, True

    try:
      date = dateparse.parse(args.date)
      #print(date)
    except:
      sys.exit(f"Error, cannot parse date '{args.date}'")

    #Use default Nones for location to check if it is Vaxjo by default
    # Decimal lat-long coordinates are 56.87767, 14.80906.
    # 56.8790° N, 14.8059° E
    if args.longitude is None and args.latitude is None: #Vaxjo by default
      args.longitude = 14.80906
      args.latitude = 56.87767
      place = "Vaxjo"
    else:
      if args.longitude is None:
        args.longitude = 14.80906
      if args.latitude is None:
        args.latitude = 56.87767
      place = "specified location"

    #Make an observer
    observer      = ephem.Observer()


    if args.Horizon == 'a': #Astro default
      horizon = '-18'
      cent = True
      description = 'Astronomical night'
    elif args.Horizon == 'n':  #Nautical twilight
      horizon = '-12'
      cent = True
      description = 'Nautical night'
    elif args.Horizon == 'c': #Civil twilight
      horizon = '-6'
      cent = True
      description = 'Civil night'
    elif args.horizon == 's': #Sunrise/Sunset 
      horizon = '0'
      cent = False
      description = 'Official night (Sunset/rise)'

    # Decimal lat-long coordinates are 56.87767, 14.80906.
    # 56.8790° N, 14.8059° E
    #observer_lat, observer_lon = 56.87767, 14.80906
    observer.lat  = str(args.latitude)      #Note that lat should be in string format
    observer.lon  = str(args.longitude) #Note that lon should be in string format
    #Elevation, in metres
    observer.elev = args.elevation

    #To get U.S. Naval Astronomical Almanac values, use these settings
    observer.pressure= 0
    #observer.horizon = '-0:34'

    observer.horizon = horizon #-6=civil twilight, -12=nautical, -18=astronomical

    #PyEphem takes and returns only UTC times
    #time_now = time.gmtime()
    #date = time.strftime("%Y-%m-%d %H:%M:%S ", time_now) 
    #date = "2020-10-20 04:00:11"

    observer.date = date

    ## Don't need this, calculate the time to rise/set from given date (which will be now by defautl)
    #t_now = dateparse.parse(time.strftime("%Y-%m-%d %H:%M:%S ",time.gmtime()))
    #t_now = ephem.date(t_now)

    if args.Sun:

      body = ephem.Sun()

      body.compute(observer)

      #print(dir(body))
      # Useful are "radius" (for Sun/Moon) and "phase" (for the Moon)    
      #print(args)


      # sun.alt prints in deg:min:sec, but represented in radians
      set = math.degrees(body.alt+body.radius*cent)<float(horizon)
      print("Yes" if set else "No", end="")
      print(" for",description)
      print("Sun altitude is",body.alt,"at",date,"in "+place)


      if set:
        try:
            change_time = observer.next_rising   (body, use_center=cent)
        except: # AlwaysDownError:
            print("Sun is always Down at this time of year, for",description.split()[0])
        else:
            print("Sun will rise ("+description.split()[0]+") at",change_time,
              ", in {:.0f}".format((change_time-observer.date)*24*60*60),"seconds from now (or -d date)")            
      else:
        try:
            change_time = observer.next_setting   (body, use_center=cent)
        except: # AlwaysUpError: AlwaysUpError name not found for exception!?
            print("Sun is always Up at this time of year, for",description.split()[0])
        else:
            print("Sun will set ("+description.split()[0]+") at",change_time,
              ", in {:.0f}".format((change_time-observer.date)*24*60*60),"seconds from now (or -d date)")

    if args.Moon:

      observer.horizon = '0' # Override that for sun
      cent = False

      body = ephem.Moon()

      body.compute(observer)

      # body.alt prints in deg:min:sec, but represented in radians
      set = math.degrees(body.alt+body.radius)<0
      print("Yes" if set else "No", end="")
      print(" the Moon is",("set" if set else "risen"))
      print("Moon altitude is",body.alt,"at",date,"in "+place+f", with phase {body.phase:.2f}%")
      # No need to check if moon rises/sets, since Moon always rises or sets
      if set:
        change_time = observer.next_rising   (body, use_center=cent)
        print("Moon will rise at",change_time,
              ", in {:.0f}".format((change_time-observer.date)*24*60*60),"seconds from now (or -d date)")
      else:
        change_time = observer.next_setting   (body, use_center=cent)
        print("Moon will set at",change_time,
              ", in {:.0f}".format((change_time-observer.date)*24*60*60),"seconds from now (or -d date)")
        #print(description.split()[0])
# -



