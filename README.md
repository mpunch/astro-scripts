# Astro Scripts repository

This contains some scripts which can be useful for astro applications

* IsSet: Tells if the sun/moon are risen or set at given location (default Vaxjo, Sweden)  (use -h for help)
* IsSet.{py,ipynb}: Notebook with the same, with the python in sync with jupytext
* astro_mp_lib: library with (for now) just the main function for the above
* Astro_Vaxjo.py: Gives sun rise/set times for Vaxjo (argument n,c,s, for Nautical,civil,Official, else Astro)

There is a binder for executing these, at:
* https://mybinder.org/v2/gl/mpunch%2Fastro-scripts/94da90400b7b6f9719d1f1d226cd21fa2063735d

For the binder, 
* click on the link, 
* let **Binder** create the environment, 
* then on the upper right ```New``` button, click on ```Terminal```
* run scripts with ```python IsSet``` or ```python Astro_Vaxjo.py```, with the arguments you wish

Or just run the IsSet notebook:
* [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mpunch%2Fastro-scripts/HEAD?filepath=IsSet.ipynb)
